from flask import Flask
import json
import time
import sqlite3
app = Flask(__name__)


measures = []

conn = sqlite3.connect('example.db')

cursor = conn.cursor()

cursor.execute('''
CREATE TABLE IF NOT EXISTS measures
(id INTEGER, measure REAL, timestamp INTEGER, PRIMARY KEY (id))
''')


def is_float(n):
    "determines if a value is a float"
    try:
        float(n)
        return True
    except:
        return False


@app.route('/measures/add/<measure>', methods=['PUT'])
def add_measure(measure):
    if is_float(measure):
        utc_millis = int(time.time() * 1000)
        id = len(measures)
        value = float(measure)
        measure = {'id': id, 'value': value, 'timestamp': utc_millis }
        measures.append(measure)
        result = { 'action': 'add', 'result': 'SUCCESS', 'measure': measure }
        json_result = json.JSONEncoder().encode(result)

        cursor.execute("INSERT INTO measures VALUES (?,?,?)", (id, value, utc_millis))
        conn.commit()

        return json_result
    else:
        result = { 'action': 'add', 'result': 'FAILURE', 'code': 'non-numeric-input' }
        json_result = json.JSONEncoder().encode(result)
        return json_result


@app.route('/measures/<id>', methods=['GET'])
def get_measure(id):
    i = (id, )
    cursor.execute("SELECT id, measure, timestamp FROM measures WHERE id=?", i)
    measure = cursor.fetchone()
    id = measure[0]
    value = measure[1]
    timestamp = measure[2]
    res = { 'id': id, 'measure': value, 'timestamp': timestamp }
    return json.JSONEncoder().encode(res)

    
@app.route('/measures/list', methods=['GET'])
def get_measures():
    results = { 'measures': [] }
    if len(measures) > 0:
        for measure in measures:
            results['measures'].append(measure)

    json_result = json.JSONEncoder().encode(results)
    return json_result
        
@app.route('/')
def hola():
    return 'Hola Gato!'


@app.route('/get', methods=['GET'])
def get():
    return '...gotten'


@app.route('/name/<name>', methods=['GET'])
def name(name):
    ret = {}
    ret['name'] = name
    ret_json = json.JSONEncoder().encode(ret)
    return ret_json

