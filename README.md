## So..

```
$ export FLASK_APP=hello.py
$ flask run
```

## Meanwhile, in another terminal...
```
$ curl -X PUT -i http://127.0.0.1:5000/measures/add/1
$ curl -X PUT -i http://127.0.0.1:5000/measures/add/2
$ curl -X PUT -i http://127.0.0.1:5000/measures/add/3
```

## Show me all of them

```
$ curl -X GET -i http://127.0.0.1:5000/measures/list
```

## Get one specific one by <id>

```
$ curl -X PUT -i http://127.0.0.1:5000/measures/<id>
```

For example id=42:

```
$ curl -X PUT -i http://127.0.0.1:5000/measures/42
```
